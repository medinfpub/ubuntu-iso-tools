# ubuntu-desktop-autoinstall-tools

Tools for creating installation media for automatic and unattended installation of Ubuntu 22.04

## Requirements

- mkautoinstalliso
  * `xorriso` (`>=1.5.3`)
  * common unix tools should be installed on most systems: `sed`, `awk`, `tr`, `mktemp`, `md5sum` `fdisk` and `dd`

- mkseediso
  * `cloud-localds`
  * `openssl`
  * common unix tools should be installed on most systems: `mktemp`

## Example testing tools with KVM

You can test these tools with virtual machines using `kvm`

- Download an ubuntu live server ISO

```
wget https://cdimage.ubuntu.com/ubuntu-server/daily-live/current/jammy-live-server-amd64.iso \
	-O iso/jammy-live-server-amd64.iso
```

- Create a new ISO modified for automatic unattended installation

```
./bin/mkautoinstalliso \
	-i iso/jammy-live-server-amd64.iso \
	-o jammy-live-server-autoinstall-amd64.iso
```

- Create an ISO to use as a cloud-init data source

```
./bin/mkseediso -n examplehostname -u exampleuser -o exampleseed.iso
```

- Create a target disk

```
truncate -s 10G image.img
```

- Run the install

```
kvm -cpu host -no-reboot -m 1024 \
	-drive file=image.img,format=raw,cache=none,if=virtio \
	-drive file=exampleseed.iso,format=raw,cache=none,if=virtio \
	-cdrom jammy-live-server-autoinstall-amd64.iso
```

- Boot the installed system

```
kvm -cpu host -no-reboot -m 1024 \
	-drive file=image.img,format=raw,cache=none,if=virtio
```

