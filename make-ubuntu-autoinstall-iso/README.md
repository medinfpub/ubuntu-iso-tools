# mkautoinstalliso

Create automatic all-in-one unattended installation ISOs for Ubuntu Server 22.04

## Example

- Download an ubuntu live server ISO

```
wget https://cdimage.ubuntu.com/ubuntu-server/daily-live/current/jammy-live-server-amd64.iso -O iso/jammy-live-server-amd64.iso
```

- Create new ISO configured for automatic unattended installation

```
./bin/mkautoinstalliso -i iso/jammy-live-server-amd64.iso -c etc/example-user-data -o jammy-live-server-autoinstall-amd64.iso
```

- Test the installer with kvm

```
truncate -s 10G image.img
kvm -no-reboot -m 1024 -cpu host -drive file=image.img,format=raw,cache=none,if=virtio -cdrom jammy-live-server-autoinstall-amd64.iso
kvm -no-reboot -m 1024 -cpu host -drive file=image.img,format=raw,cache=none,if=virtio
```